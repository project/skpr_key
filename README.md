# Skpr key

This Drupal module adds a new key provider for the [Key module](https://www.drupal.org/project/key) - it allows you to
use configuration from Skpr using [skpr/config](https://github.com/skpr/php-config).

# Usage

- Enable the `skpr_key` module.
- Add a new key entity with the `Skpr` provider.
- Configure the provider with the config key.
