<?php

namespace Drupal\skpr_key;

use Drupal\Core\Config\ConfigFactoryInterface;
use Skpr\SkprConfig;

/**
 * Class SkprConfigFactory.
 *
 * Build SkprConfig instances as required.
 */
class SkprConfigFactory {

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Creates a new ModuleConfigOverrides instance.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface|null $config_factory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * Create a SkprConfig instance.
   *
   * @param string $filename
   *   The filename to load.
   *
   * @return \Skpr\SkprConfig
   *   The SkprConfig instance.
   */
  public function getInstance($filename = NULL) {
    if (empty($filename)) {
      $filename = $this->configFactory->get('skpr_key.settings')->get('filename');
    }
    if (empty($filename)) {
      $filename = SkprConfig::CONFIG_FILENAME;
    }
    return SkprConfig::create()->load($filename);
  }

}
