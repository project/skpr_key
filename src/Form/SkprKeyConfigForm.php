<?php

namespace Drupal\skpr_key\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Provides a config form for Skpr key.
 */
class SkprKeyConfigForm extends ConfigFormBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['skpr_key.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'skpr_key.config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('skpr_key.settings');

    $form['filename'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Skpr configuration file path'),
      '#description' => $this->t('The Skpr config file location. Leave empty to use the default /etc/skpr/data/config.json.'),
      '#default_value' => $config->get('filename'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('skpr_key.settings');
    $config
      ->set('filename', $form_state->getValue('filename'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
