<?php

declare(strict_types=1);

namespace Drupal\skpr_key\Plugin\KeyProvider;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormStateInterface;
use Drupal\key\KeyInterface;
use Drupal\key\Plugin\KeyPluginFormInterface;
use Drupal\key\Plugin\KeyProviderBase;
use Skpr\SkprConfig;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Adds a key provider that allows a key to be loaded from Skpr config.
 *
 * @KeyProvider(
 *   id = "skpr",
 *   label = "Skpr",
 *   description = @Translation("This provider retrieves the key from Skpr config."),
 *   storage_method = "skpr",
 *   key_value = {
 *     "accepted" = FALSE,
 *     "required" = FALSE
 *   }
 * )
 */
class SkprKeyProvider extends KeyProviderBase implements KeyPluginFormInterface {

  /**
   * The Skpr config object.
   *
   * @var \Skpr\SkprConfig
   */
  protected SkprConfig $skpr;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    /** @var self $instance */
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->skpr = $container->get('skpr_key.skpr_config_factory')->getInstance();
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'skpr_key' => '',
      'base64_encoded' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration(): array {
    // Ensure keys always exist to reduce errors across version upgrades.
    return array_merge($this->defaultConfiguration(), parent::getConfiguration());
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    ['skpr_key' => $skpr_key, 'base64_encoded' => $base64_encoded] = $this->getConfiguration();

    $form['skpr_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Skpr key'),
      '#description' => $this->t('The name of the skpr config to load.'),
      '#required' => TRUE,
      '#empty_value' => '',
      '#default_value' => $skpr_key,
    ];

    // If this key type is for an encryption key.
    if ($form_state->getFormObject()->getEntity()->getKeyType()->getPluginDefinition()['group'] == 'encryption') {
      // Add an option to indicate that the value is stored Base64-encoded.
      $form['base64_encoded'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Base64-encoded'),
        '#description' => $this->t('Check this if the key value is Base64-encoded.'),
        '#default_value' => $base64_encoded,
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void {
    ['skpr_key' => $skpr_key, 'base64_encoded' => $base64_decode] = array_merge($this->defaultConfiguration(), $form_state->getValues());
    if ($this->isMultiKeyValue($skpr_key)) {
      if (!$this->getMultiKeyValue($skpr_key, $base64_decode)) {
        $form_state->setErrorByName('skpr_key', $this->t("The multi-value skpr key doesn't exist."));
      }
      return;
    }
    if (!$this->getSingleKeyValue($skpr_key, $base64_decode)) {
      $form_state->setErrorByName('skpr_key', $this->t("The skpr key doesn't exist."));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->setConfiguration($form_state->getValues());
  }

  /**
   * A method to determine whether the key value is multi.
   *
   * This is done by just checking the end of the string for .*.
   *
   * @param string $skpr_key
   *   The skpr key.
   *
   * @return bool
   *   The result.
   */
  protected function isMultiKeyValue(string $skpr_key): bool {
    return str_ends_with($skpr_key, '.*');
  }

  /**
   * A method to get a multi key value.
   *
   * @param string $skpr_key
   *   The skpr key.
   * @param bool $base64_decode
   *   Whether to base 64 decode.
   *
   * @return string
   *   The JSON encoded key value.
   */
  protected function getMultiKeyValue(string $skpr_key, bool|int $base64_decode = FALSE): string {
    $key_values = [];
    $skpr_key_values = array_filter($this->skpr->getAll(), fn($key) => str_starts_with($key, substr($skpr_key, 0, -1)), ARRAY_FILTER_USE_KEY);
    foreach ($skpr_key_values as $key => $value) {
      if ($base64_decode) {
        $value = $this->base64Decode($value);
      }
      $key_values[substr($key, strlen($skpr_key) - 1)] = $value;
    }
    return Json::encode(array_filter($key_values));
  }

  /**
   * A method to get a single key value.
   *
   * @param string $skpr_key
   *   The skpr key.
   * @param bool $base64_decode
   *   Whether to base 64 decode.
   *
   * @return mixed
   *   The key value.
   */
  protected function getSingleKeyValue(string $skpr_key, int|bool $base64_decode = FALSE): mixed {
    $key_value = $this->skpr->get($skpr_key, '');
    if ($base64_decode) {
      $key_value = $this->base64Decode($key_value);
    }
    return $key_value;
  }

  /**
   * A method to base64 decode the value if necessary.
   *
   * @param mixed $value
   *   The value.
   *
   * @return string
   *   The decoded value.
   */
  protected function base64Decode(mixed $value): string {
    if (!is_string($value)) {
      return '';
    }
    $value_decoded = base64_decode($value, TRUE);
    if (!$value_decoded || preg_match('/[^\x20-\x7e]/', $value_decoded)) {
      return '';
    }
    return $value_decoded;
  }

  /**
   * {@inheritdoc}
   */
  public function getKeyValue(KeyInterface $key): mixed {
    ['skpr_key' => $skpr_key, 'base64_encoded' => $base64_decode] = $this->getConfiguration();
    if ($this->isMultiKeyValue($skpr_key)) {
      return $this->getMultiKeyValue($skpr_key, $base64_decode);
    }
    return $this->getSingleKeyValue($skpr_key, $base64_decode);
  }

}
