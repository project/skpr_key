<?php

declare(strict_types=1);

namespace Drupal\Tests\skpr_key\Unit;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\key\Entity\Key;
use Drupal\key\Plugin\KeyPluginManager;
use Drupal\skpr_key\Plugin\KeyProvider\SkprKeyProvider;
use Drupal\skpr_key\SkprConfigFactory;
use Drupal\Tests\UnitTestCase;
use Skpr\SkprConfig;

/**
 * @coversDefaultClass \Drupal\skpr_key\Plugin\KeyProvider\SkprKeyProvider
 * @group skpr_key
 */
class SkprKeyPluginTest extends UnitTestCase {

  /**
   * Container builder.
   *
   * This should be used sparingly by test cases to add to the container as
   * necessary for tests.
   */
  protected ContainerBuilder $container;

  /**
   * Provides a config provider.
   */
  public function configProvider(): array {
    return [
      // ['key_name', 'expected_value', 'base64_encoded'].
      ['my_test.config', 'test_value', FALSE],
      ['my_test.encoded', 'happy days', TRUE],
      ['my_test.integer', 5, FALSE],
      ['my_test.not_set', '', FALSE],
      ['my_test.multi.*', '{"foo":1,"bar":"test"}', FALSE],
      ['my_test.multi.*', '[]', TRUE],
      ['my_test.multi_encoded.*', '{"foo":"happy","bar":"days"}', TRUE],
      ['my_test.deep.*', '{"a.a":"apple","a.b":"banana","b":"kiwi"}', FALSE],
    ];
  }

  /**
   * A test in getting single and multiple skpr values using the provider.
   *
   * @dataProvider configProvider
   */
  public function testSkprValue($key_name, $expected, $encoded): void {
    $key_settings = [
      'skpr_key' => $key_name,
      'base64_encoded' => $encoded,
    ];

    $definition = [
      'id' => 'skpr',
      'label' => 'Skpr',
      'storage_method' => 'skpr',
    ];
    $provider = SkprKeyProvider::create($this->container, $key_settings, 'skpr', $definition);

    $values = [
      'key_id' => $this->getRandomGenerator()->word(15),
      'key_provider' => 'skpr',
      'key_provider_settings' => $key_settings,
    ];
    $key = new Key($values, 'key');
    $this->assertEquals($expected, $provider->getKeyValue($key), 'Value of Skpr key returned as expected.');
  }

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->container = new ContainerBuilder();

    $skpr = SkprConfig::create()->load(__DIR__ . '/../../assets/config.json');

    // Set up the skpr config factory service.
    $skpr_config_factory = $this->prophesize(SkprConfigFactory::class);
    $skpr_config_factory->getInstance()->willReturn($skpr);
    $this->container->set('skpr_key.skpr_config_factory', $skpr_config_factory->reveal());

    // Set up the key provider plugin manager.
    $key_plugin_provider = $this->prophesize(KeyPluginManager::class);
    $key_plugin_provider->getDefinitions()->willReturn([
      [
        'id' => 'skpr',
        'label' => 'Skpr',
        'storage_method' => 'skpr',
      ],
    ]);
    $this->container->set('plugin.manager.key.key_provider', $key_plugin_provider);

    \Drupal::setContainer($this->container);
  }

}
